# Bibliotheken

## Backend
 * https://www.npmjs.com/package/text-diff
   * zur Bestimmung des Unterschieds zwischen Server und Client Text
   * [Demo](https://neil.fraser.name/software/diff_match_patch/demos/diff.html)
   * [Offizielle API](https://github.com/google/diff-match-patch/wiki/API)
    

## Backend & Frontend
 * https://www.npmjs.com/package/checksum
   * zur Bestimmung der Prüfsumme über den gesamten Text
